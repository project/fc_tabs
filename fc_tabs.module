<?php
/**
 * @file
 * Provides widget for field collections on tabs
 */
/**
 * Implements hook_field_formatter_info().
 */
function fc_tabs_field_formatter_info() {
  return [
    'field_collection_tabs' => [
      'label' => t('Tabs field collection'),
      'description' => t('Field collection form elements as tabs'),
      'field types' => [
        'field_collection',
      ],
      'settings' => [
        'title_field' => 0,
        'type' => 'vertical_tabs',
        'view_mode' => 'full',
      ],
    ],
  ];
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function fc_tabs_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $element = [];

  if ($display['type'] == 'field_collection_tabs') {
    $options = [t('No titles')];
    $fields = field_info_instances('field_collection_item', $field['field_name']);
    foreach ($fields as $field_name => $field) {
      $options[$field_name] = $field['label'];
    }
    $element['title_field'] = [
      '#type' => 'select',
      '#title' => t('Field to use for tab titles'),
      '#description' => t('Select the field to use for tab titles'),
      '#default_value' => $settings['title_field'],
      '#options' => $options,
    ];
    $element['type'] = [
      '#type' => 'select',
      '#title' => t('Type'),
      '#default_value' => $settings['type'],
      '#options' => [
        'vertical_tabs' => t('Vertical tabs'),
        'horizontal_tabs' => t('Horizontal tabs'),
      ],
    ];
    $entity_type = entity_get_info('field_collection_item');
    $options = [];
    foreach ($entity_type['view modes'] as $mode => $info) {
      $options[$mode] = $info['label'];
    }

    $element['view_mode'] = [
      '#type' => 'select',
      '#title' => t('View mode'),
      '#options' => $options,
      '#default_value' => $settings['view_mode'],
      '#description' => t('Select the view mode'),
    ];
  }
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function fc_tabs_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  if ($display['type'] == 'field_collection_tabs') {
    $fields = field_info_instances('field_collection_item', $field['field_name']);
    if (!empty($fields[$settings['title_field']])) {
      return t('Title field: %title_field', [
        '%title_field' => $fields[$settings['title_field']]['label'],
      ]);
    }
    else {
      return t('No title');
    }
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function fc_tabs_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = [];

  switch ($display['type']) {
    // This formatter simply outputs the net_field/period wrapped in a div.
    case 'field_collection_tabs':
      $title_field = !empty($display['settings']['title_field']) ? $display['settings']['title_field'] : FALSE;
      $type = !empty($display['settings']['type']) ? $display['settings']['type'] : FALSE;
      $view_mode = !empty($display['settings']['view_mode']) ? $display['settings']['view_mode'] : 'full';

      $form[$title_field] = [
        '#type' => 'vertical_tabs',
      ];
      $content_list = $title_list = [];
      foreach ($items as $delta => $item) {
        if ($field_collection = field_collection_field_get_entity($item)) {
          $content = $field_collection->view($view_mode);
          $tab_id = drupal_clean_css_identifier($field['field_name'] . '-tab-' . $delta);
          if ($title_field && ($render_array = $content['field_collection_item'][$field_collection->item_id])) {
            $form[$title_field][$tab_id] = [
              '#type' => 'fieldset',
              '#title' => $title = drupal_render($render_array[$title_field]),
              '#group' => $title_field,
            ];
            $title_list[] = '<a data-toggle="tab" href="#' . $tab_id . '">' . $title . '</a>';
            $render_array[$title_field]['#access'] = FALSE;
            $form[$title_field][$tab_id]['field_collection_content'] = [
              '#type' => 'markup',
              '#markup' => $content_tabs = drupal_render($render_array),
            ];
            $first_class = empty($content_list)?'in active':'';
            $content_list[] = [
              '#theme' => 'html_tag',
              '#tag' => 'div',
              '#attributes' => [
                'class' => ['tab-pane', 'fade', $first_class],
                'id' => $tab_id,
              ],
              '#value' => $content_tabs,
            ];
          }
        }
      }
      if ($type == 'horizontal_tabs') {
        $form = [];
        $form['list'] = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $title_list,
          '#attributes' => ['class' => ['nav', 'nav-tabs']],
        ];
        $form['tab-content'] = [
          '#theme' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => ['class' => ['tab-content']],
          '#value' => drupal_render($content_list),
        ];
      }
      $chidren = drupal_render($form);
      $element[] = [
        '#theme' => 'container',
        '#field_name' => $field['field_name'],
        '#prefix' => '<div class="field-collection-tabs ' . $type . '">',
        '#suffix' => '</div>',
        '#children' => $chidren,
      ];
      break;

  }
  //@todo add + button remove button foreach item
  return $element;
}
